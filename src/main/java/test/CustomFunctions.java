package test;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CustomFunctions {
	
	public static WebDriver driver;

	public static void test()  {
		//setting and creating webdriver instance
		System.setProperty("webdriver.chrome.driver", ".\\src\\main\\resources\\utility\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		//navigating to site
		driver.get("https://www.imdb.com/");

		//action items
		WebElement element= driver.findElement(By.xpath(OR.watchList));
		
		Actions action = new Actions(driver);
		action.moveToElement(element).build().perform();
		WebDriverWait wait = new WebDriverWait(driver,30);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(OR.arrow)));
		driver.findElement(By.xpath(OR.arrow)).click();

		//Initialization of lists
		List<WebElement> movieName = new ArrayList<WebElement>();
		List<WebElement>  movieY = new ArrayList<WebElement>();
		List<Integer>  movieYear = new ArrayList<Integer>();
		List<WebElement>  movieRating = new ArrayList<WebElement>();

		//Fetching values from page
		movieName =driver.findElements(By.xpath(OR.movieNames));
		movieY=driver.findElements(By.xpath(OR.movieYear));
		movieRating=driver.findElements(By.xpath(OR.rating));

		//inserting data into database
		for(int i=0;i<250;i++)
		{
			String x= movieY.get(i).getText();
			x=x.substring(1, 5);
			movieYear.add(Integer.parseInt(x));
			insert(movieName.get(i).getText(), movieYear.get(i), movieRating.get(i).getText());
		}
		driver.quit();


	}
	
	public static void createNewDatabase(String fileName) {

		String url = "jdbc:sqlite:.\\src\\main\\resources" + fileName;

		try (Connection conn = DriverManager.getConnection(url)) {
			if (conn != null) {
				DatabaseMetaData meta = conn.getMetaData();
				System.out.println("The driver name is " + meta.getDriverName());
				System.out.println("A new database has been created.");
			}

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static void createNewTable() {
		// SQLite connection string
		String url = "jdbc:sqlite:.\\src\\main\\resources\\tests.db";

		// SQL statement for creating a new table
		String sql = "CREATE TABLE IF NOT EXISTS Movies (\n"
				+ "	id integer PRIMARY KEY,\n"
				+ "	moviename text NOT NULL,\n"
				+ " movieYear real,\n"
				+ "	rating real\n"
				+ ");";

		try (Connection conn = DriverManager.getConnection(url);
				Statement stmt = conn.createStatement()) {
			// create a new table
			stmt.execute(sql);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static Connection connect() {
		// SQLite connection string
		String url = "jdbc:sqlite:.\\src\\main\\resources\\tests.db";
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(url);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return conn;
	}
	
	public static void insert(String movieName, int movieYear,String rating) {
		String sql = "INSERT INTO Movies(moviename,movieYear,rating) VALUES(?,?,?)";

		try (Connection conn = connect();
				PreparedStatement pstmt = conn.prepareStatement(sql)) {
			pstmt.setString(1, movieName);
			pstmt.setInt(2, movieYear);
			pstmt.setString(3, rating);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static void selectAll(){
		String sql = "SELECT id, moviename, movieYear,rating FROM Movies";

		try (Connection conn = connect();
				Statement stmt  = conn.createStatement();
				ResultSet rs    = stmt.executeQuery(sql)){

			// loop through the result set
			while (rs.next()) {
				System.out.println(rs.getInt("id") +  "\t" + 
						rs.getString("moviename") + "\t" +
						rs.getInt("movieYear") + "\t" +
						rs.getString("rating"));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	public static void delete(int id) {
		String sql = "DELETE FROM Movies WHERE id = ?";

		try (Connection conn = connect();
				PreparedStatement pstmt = conn.prepareStatement(sql)) {

			// set the corresponding param
			pstmt.setInt(1, id);
			// execute the delete statement
			pstmt.executeUpdate();

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	public static void drop() {
		String sql = "DROP TABLE IF EXISTS Movies";

		try (Connection conn = connect();
				PreparedStatement pstmt = conn.prepareStatement(sql)) {

			pstmt.executeUpdate();
			System.out.println("Table dropped");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

}
