package test;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Driverscript {
	
	@BeforeMethod
	public void beforeMethod() {
		CustomFunctions.createNewDatabase("test.db");
		CustomFunctions.createNewTable();
	}
	
	@Test
	public void executeMethod() {
		CustomFunctions.test();
		//CustomFunctions.drop();
	}

	@AfterMethod
	public void afterMethod() {
		CustomFunctions.selectAll();
	}
}
